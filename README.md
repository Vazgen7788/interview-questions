# Interview Tasks. You have 2 hours.

### Open editor and implement each task in separate files. 
### Feel free to use internet for tasks implementation.
### Order of tasks implementation doesn't matter.

1) Implement a function, without using if, switch-case and any other condition checker operator.

```js
function foo() {

}

console.log(foo(0) === 1); // output must be true
console.log(foo(1) === 0); // output must be true
```

---------------------------------------------------------
2) Create a function which accepts number, for example X number.
Function will calculate and return arithmetic average of range  prime numbers from 0 - X;
Ստեղծել ֆունկցիա, որը արգումենտ կընդունի թիվ (X)։ Ֆունկցիան պետկ է հաշվի և արտածի 0-X ընկած
միջակայքի պարզ թվերի միջին թվաբանականը։

```js
function foo(x) {
	
}
```
---------------------------------------------------------
3) 
Գրել ֆունկցիայի մարմինը։ Ֆունկցիան պետք է վերադարձնի զանգվածի չկրկնվող, դրական թվերը։

```js
const arr = [5, 4, 5, 'jon', 7, 4, null, {}, [], NaN, 8, 9, 8];

function bar(arr) {
	
} 

const result = bar(arr);
conosle.log(result);
```
---------------------------------------------------------
4) Implement function without using loops. It must return reversed string.

```js
function foo(str) {

}

const str = 'john-doe';
const result = foo(str);
console.log(result); // ouput must be 'eod-nhoj'

```

---------------------------------------------------------
5) Implement listener function for class.

```js
class Ball {
	constructor() {
		this.position = 0;
	}

	move() {
		this.position++;
	}

    onMove() {
        // write functions body
    }
}

const ball = new Ball();
// // callback must work on every single move
// ball.onMove(position => {
// 	   console.log(position);
// });

let i = 10;
while(i--) {
	ball.move();	
}
// should console log 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
```
---------------------------------------------------------


7) Implement functions `downloadFilesParallel` and `downloadFilesStepByStep`;

`downloadFilesParallel` will download all files together at the same time.

`downloadFilesStepByStep` will download files by turn, meaning one after another. For example 
when first file downloaded then will start second file download.

```js

function downloadFile(fileName) {
	return new Promise(resolve => {
		console.log(`* downloading ${fileName}`);
		setTimeout(() => {
			console.log(`✅ Downloaded ${fileName} !!!`);
			resolve();
		}, 2000);
	});
}

const files = ['file-1.pdf', 'file-2.pdf', 'file-3.pdf', 'file-4.pdf'];


function downloadFilesParallel(files) {

}

function downloadFilesStepByStep(files) {

}


downloadFilesParallel(files).then(() => {
	console.log('Files Downloaded parallel 👏🏻');

	downloadFilesStepByStep(files).then(() => {
		console.log('Files downloaded step by step 👏🏻')
	});
});

```


[Task 1](https://docs.google.com/presentation/d/1eZV_a_c5YLOEGQzAZ94C0Z1UP5zlY-4fHM0e4yOYcOk/edit?usp=sharing)

[Task 2](https://docs.google.com/presentation/d/1nelcNaLpLJ54N77Ot9b5dUDVgF-VunnFsdOfkTJxIL8/edit?usp=sharing) (Work on this if you complete all other tasks)
